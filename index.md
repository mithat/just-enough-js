% Just Enough JavaScript
% Mithat Konar

Notes for a crash course in JavaScript.

* [1 : Introduction](just-enough-js-01.html)
* [2 : Output, Input, Storage, Data types](just-enough-js-02.html)
* [3 : Processing](just-enough-js-03.html)
* [4 : Functions](just-enough-js-04.html)
* [5 : Arrays and Objects](just-enough-js-05.html)

<script>
window.onload = function () {
    activeNavLink('navhome');
}
</script>

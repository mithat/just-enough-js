% Just Enough JavaScript / 1 /
% Mithat Konar
%

Introduction
----------------

### Intention
* This is an essentially (but not rigorously) correct crash course in JavaScript.
* It is intended to teach you just enough JavaScript so that you can start to program games using open source HTML5 game frameworks such as [enchant.js](http://enchantjs.com/), [Phaser](http://phaser.io/), and [melonJS](http://melonjs.org/).
* It assumes you don't know anything about programming.

### What is a computer system?
![](images/computer-sys.png)

* Computer systems are made up of:
    * Input
    * Output
    * Processing
    * Storage  
* Some programming concepts built on top of computer systems include:
    * Functions
    * Data structures
    * Objects

### Our approach
* We will explore each of the above in the context of JavaScript programming in the following order:
    1. Output
    1. Input
    1. Storage
    1. Processing
    1. Functions
    1. Data Structures
    1. Objects
* When discussing the first concepts, we may have to introduce tiny bits of other concepts, but we'll keep it to an absolute minimum.

### Expectations
* You won't be able to do any decent programming until you've made it part of the way through _Processing_---so be patient!
* By the time you've finished _Processing_, you'll be able to write a simple text-based role-playing game.
* By the time you've finished _Functions_ and _Data Structures_, you will be able to write more complex text-based or number-based games.
* When you have finished _Objects_, you will be able to structure your code so that you can write even more complex games.

<script>
window.onload = function () {
    activeNavLink('nav01');
}
</script>

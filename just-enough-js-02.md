% Just Enough JavaScript / 2 /
% Mithat Konar
%

Output
------
### Output to popup boxes
* The easiest and most instantly gratifying way to produce output is to use **popup boxes**.
* JavaScript has three kinds of popup boxes:
    * **alert**---Just tell the user something.
<script>
makeTextArea({
    code: ["window.alert('All your base are belong to us.');"],
    id: "out-alert"
});
</script>

    * **confirm**---Ask the user an OK/Cancel question.
<script>
makeTextArea({
    code: ["window.confirm('Destroy all monsters?');"],
    id: "out-confirm"
});
</script>

    * **prompt**---Ask the user to enter some text information.
<script>
makeTextArea({
    code: ["window.prompt('What is your name?', 'First Last');"],
    id: "out-prompt"
});
</script>

    * We will learn how to tell which button the user pressed and how to get the text the user entered a little later.
* Each instruction above is a **statement**---the computer language equivalent of a sentence. In JavaScript statements must have a semicolon (`;`) at the end.

### Output to the console
* The **console** is a text-only area that is normally hidden from the user.
* Writing output to the console is usually used only for _debugging_ (fixing problems in the program).
* In Firefox or Chrome, you open the console by right clicking on a page, then selecting _Inspect Element_, then clicking the _Console_ button or tab.
    * In Firefox, there are several "sub-consoles". We are only interested in the _Logging_ option, so disable the other ones.
* You can send output text to the console using the `console.log` function:
<script>
makeTextArea({
    code: ["console.log('I want a jellybean.');"],
    id: "out-console"
});
</script>
* If you don't see anything when you click "Run", it's probably because you didn't open your browser's console panel.

### Output to an HTML5 page
* JavaScript is very often used to write output to an HTML page.
* We will not cover how to do this because it's not useful for our purposes.

### Escape sequences
* What if you wanted to output something in an alert with more than one line?
* To do this, you need to use a special sequence of characters (an **escape sequence**) that tells JavaScript that you want to start a new line:
<script>
makeTextArea({
    code: ["window.alert('I want a jellybean.\\nAnd I want it now!');"],
    id: "out-escape"
});
</script>

* JavaScript has many escape sequences including:
    * **`\n`** start a new line
    * **`\t`** horizontal tab
    * **`\\`** backwards slash
    * **`\'`** single quote
    * **`\"`** double quote
* You will see that the escape sequences for single and double quote are pretty useful when we talk more about character strings.

### Comments
* While comments are not really related to output, we think this is a good place to introduce them.
* A **comment** is essentially a chunk of text in your program that is visible to you or a reader but is not noticed by the JavaScript interpreter.
<script>
makeTextArea({
    code: [
        "// This line is a comment. It won't have any affect on the program.",
        "window.alert('This bit is not a comment.')"
    ],
    id: "comments-01"
});
</script>

* There are two ways to indicate something is a comment in JavaScript:
    * **double-slash comment**---everything to the end of the line is a comment.
<script>
makeTextArea({
    code: [
        "// This is a double-slash comment.",
        "window.alert('Eat more oranges.');"
    ],
    id: "comments-doubleslash"
});
</script>

    * **slash-star comment**---everything between `/*` and `*/` is a comment. It can span multiple lines. (Sometimes called "C-style" or "multi-line" comments.)
<script>
makeTextArea({
    code: [
        "/* This is a slash-star comment. The comment ends",
        "after the star-slash. */ window.alert('This is not.');"
    ],
    id: "comments-slashstar"
});
</script>

* Comments are often used to add information about who wrote the code and when it was written as well as to describe what the code is doing if it's not obvious.
<script>
makeTextArea({
    code: [
        "/* This bit of awesomeness was written by Mithat Konar.",
        "   It was written on January 6, 2014, a brutally cold night. */",
        "",
        "window.alert('Brrrr...');   // Write a message in a popup box."
    ],
    id: "comments-example"
});
</script>

Input
-----
### Input from popup boxes
* To use the input provided by the user in confirm and prompt popup boxes, you need to store the result of the popup in a **variable** (discussed more completely later).
* In both of the following examples, we create a variable named `resp` to store the input provided by the user and then show that in a new popup.
    * **confirm**:
<script>
makeTextArea({
    code: [
        "var resp = window.confirm('Destroy all monsters?');",
        "window.alert(resp);",
    ],
    id: "in-confirm"
});
</script>

    * **prompt**:
<script>
makeTextArea({
    code: [
        "var resp = window.prompt('What is your name?', 'First Last');",
	"window.alert(resp);"
    ],
    id: "in-prompt"
});
</script>

### Input from an HTML page
* JavaScript can use the data entered into forms and other elements on an HTML page as input.
* We will not cover how to do this because it's not useful for our purposes.

Storage
-------
### Variables
* A **variable** is a place with a name in computer memory where you can store values.
* In the example below, we create (or **declare**) a variable named `x`, then we store the character string `'OMG!'` in `x` and output the value stored in `x` to the user.
* The special term `var` tells JavaScript to make variables with the name(s) that follow.
<script>
makeTextArea({
    code: [
	"var x;\nx = 'OMG!';\nwindow.alert(x);"
    ], 
    id: "variables-01"
});
</script>

* Notice how when we wrote the variable name in the alerts, it produced the value stored in the variable, just like you might expect from a mathematical function.
* The value stored in a variable can change. That's why it's called a _vari_able!
<script>
makeTextArea({
    code: [
	"var x;",
	"x = 'OMG!';",
	"window.alert(x);",
	"x = 'LOLZ!';",
	"window.alert(x);",
	"x = 'This is awesome!';",
	"window.alert(x);"
    ],
    id: "variables-02"
});
</script>

### Variable names
* In the preceding example, we gave our variable a very simple name, `x`.
* Variable names in JavaScript are usually longer than a single character.
* You should try to use variable names that help describe what is stored in the variable. This makes reading a program easier.
<script>
makeTextArea({
    code: [
	"var myExclamation;",
	"myExclamation = 'OMG!';",
	"window.alert(myExclamation);"
    ], 
    id: "variable-names-01"
});
</script>

### Rules of the name
* A variable's name can't be one of the words [reserved by JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Reserved_Words).
* Spaces are not allowed.
* The first character must be a letter, an underscore (`_`) character, or the dollar sign (`$`).
* Subsequent characters can be letters, numbers, underscores (_), or dollar signs (`$`).
* Names are case sensitive (`score` and `Score` are different).
* Examples:
    * OK: `num_lives`, `numLives`, `_lives`, `$remaining`, `Player1`
    * Bad: `num-lives`, `num lives`, `50cent`, `you&me`

### Using variables
* You can create as many variables as you need in one go. Separate the variable names with commas.
<script>
makeTextArea({
    code: [
	"var myExclamation, yourExclamation;",
	"myExclamation = 'OMG!';",
	"yourExclamation = 'LOLZ!';",
	"window.alert(myExclamation);",
	"window.alert(yourExclamation);"
    ],
    id: "using-variables-01"
});
</script>

* You can initialize the value of a variable when you declare (create) it.
<script>
makeTextArea({
    code: [
        "var myExclamation = 'OMG!';",
        "window.alert(myExclamation);",
        "myExclamation = 'ROFL!';",
        "window.alert(myExclamation);"
    ],
    id: "using-variables-02"
});
</script>

### Whitespace
* The topic of whitespace isn't directly related to variables, but since we are talking about the details of using names, it's a decent time to bring it up.
* Spaces, line breaks, and horizontal tabs are catagorized as **whitespace** in JavaScript.
* The rules of whitespace in JavaScript say that:
    * There are places where you _must_ use whitespace:
<script>
makeTextArea({
    code: [
	"var myExclamation = 'Ponies!';    // Won't work without whitespace between 'var' and 'myExclamation'.",
	"window.alert(myExclamation);"
    ],
    id: "whitespace-01"
});
</script>

    * There are places where you _may_ use whitespace:
<script>
makeTextArea({
    code: [
	"/* Whitespace is not required between statements. It is optional",
	"   around identifiers, operators, brackets, and semicolons. */",
	"var myExclamation='Unicorns!';window.alert  (  myExclamation  );"
    ],
    id: "whitespace-02"
});
</script>

    * And there are places where you _may not_ use any whitespace (e.g., within a name):
<script>
makeTextArea({
    code: [
        "var myExclamation = 'Rainbows!';",
        "window.alert(my Exclamation);   // Won't work because of space in 'myExlamation'"
    ],
    id: "whitespace-03"
});
</script>

    * Where you _may_ or _must_ use whitespace, you can use as much and of any kind you want.
<script>
makeTextArea({
    code: [
        "var         myExclamation = ",
	"            'OMG!';",
	"",
        "window.alert(myExclamation);"
    ],
    id: "whitespace-04"
});
</script>

Data types
----------
* In the preceding example, we stored a string of characters in a variable.
<script>
makeTextArea({
    code: [
	"var myExclamation;",
	"myExclamation = 'OMG!';",
	"window.alert(myExclamation);"
    ],
    id: "storage-03"
});
</script>
* Character strings are but one type of data that can be stored in variables.
* You can store the following types of values in variables:
    * strings 
    * numbers
    * Boolean values
    * The special values `undefined` and `null`.
    * References to more complex _arrays_ and _objects_, which we will talk about later.

### Strings
* A string (or String or character string) in JavaScript is a series of characters taken as a unit.
* In the following example, the variable `myString` stores a string.
<script>
makeTextArea({
    code: [
	"var myString;",
	"myString = 'I am not a robot.';",
	"window.alert(myString);"
    ], 
    id: "storage-string1"
});
</script>

* The stuff on the right hand side of the equals sign is a **string literal**---a string that is _not_ a variable.
* String literals are indicated by _single_ or _double_ quotes.
<script>
makeTextArea({
    code: [
	"var herMessage = 'I have enough rope.';",
	"var hisMessage = 'Please call an ambulance.';",
	"window.alert(herMessage);",
	"window.alert(hisMessage);"
    ], 
    id: "storage-string2"
});
</script>

* If you need to use a single quote in a string literal, you can wrap it in double quotes or vice-versa.
<script>
makeTextArea({
    code: [
	"var greeting = \"That's cool!\";",
	"window.alert(greeting);",
	"",
	"greeting = 'I said, \"Hello.\"';",
	"window.alert(greeting);"
    ], 
    id: "storage-string4"
});
</script>

* You can put anything you can type in a string. You may have to use escape sequences for some characters though.

### Numbers
* You can store both integers and floating point numbers, positive and negative in a JavaScript variable.
<script>
makeTextArea({
    code: [
        "var luckyNumber = 7;           // integer (positive)",
        "window.alert(luckyNumber);",
        "",
        "luckyNumber = -22;             // integer (negative)",
        "window.alert(luckyNumber);",
		"",
        "var acceleration = 31.23;      // floating point (positive)",
        "window.alert(acceleration);",
        "",
        "acceleration = -0.33333;       // floating point (negative)",
        "window.alert(acceleration);"
    ],
    id: "storage-numerical1"
});
</script>

* Note that<br />
`var luckyNumber = 32;`{.javascript}<br />
and<br />
`var luckyNumber = '32';`{.javascript}<br />
are not the same thing. The first is an number, the second is a string.
* There are several ways to write **numerical literals**:
<script>
makeTextArea({
    code: [
        "var num;",
	"num = 3;       // simple integer",
	"num = -1.234;  // simple floating point",
	"num = 3.01e3;  // scientific notation",
	"num = 0266;    // octal",
	"num = 0xFF;    // hexadecimal",
    "",
	"window.alert(num);"
    ],
    id: "storage-numerical3"
});
</script>

### Booleans
* A Boolean type can have one of two values: `true` or `false`.
<script>
makeTextArea({
    code: [
        "var javascriptRocks = true;",
        "window.alert(javascriptRocks);",
        "",
        "javascriptRocks = false;",
        "window.alert(javascriptRocks);"
    ],
    id: "storage-Boolean1"
});
</script>

* Note that<br />
`var javascriptRocks = true;`{.javascript}<br />
and<br />
`var javascriptRocks = 'true';`{.javascript}<br />
are not the same thing. The first is a Boolean, the second is a string.

### Special values
* A variable that has been declared but not set to anything has a value	`undefined`.
<script>
makeTextArea({
    code: [
        "var myVar;",
        "window.alert(myVar);"
    ],
    id: "special-values1"
});
</script>

* You can set a variable to the special value `null` if you want it to have "no value".
<script>
makeTextArea({
    code: [
        "var myVar = 6;",
	"myVar = null;",
        "window.alert(myVar);"
    ],
    id: "special-values2"
});
</script>

### Dynamic typing
* JavaScript variables are **dynamically typed**. This means that they will store whatever type of data you want to store in them on the fly:
<script>
makeTextArea({
    code: [
        "var myVar = 7;          // now a number",
        "window.alert(myVar);",
        "",
        "myVar = 'Yo!';          // now a string",
        "window.alert(myVar);",
        "",
        "myVar = true;           // now a Boolean",
        "window.alert(myVar);"
    ],
    id: "storage-dynamic1"
});
</script>

### Storage to disc or database
* JavaScript can also be used to store data into files and databases.
* We will not cover this because it's not useful for our purposes.

<script>
window.onload = function () {
    activeNavLink('nav02');
}
</script>

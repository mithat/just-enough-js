% Just Enough JavaScript / 3 /
% Mithat Konar
%

Processing
----------
### Operations
* **Operations** are things you do with or to values that produce a new value. (Functions can do this too, but we'll talk about functions later.)
* They involve **operators** (the thing that specifies what to do) and **operands** (one or more values that will be used in the operation).
* Operands can be literals or variables (or other things as well that we might cover later).
* There are many kinds of operations.

#### Arithmetic operations
* Example of an arithmetic operation: `a + 3`
* The operator is `+` and the operands are `a` and `3`.

##### Basic arithmetic operators
* Addition, subtraction, multiplication, division, modulus.
<script>
makeTextArea({
    code: [
        "// Addition",
        "var a = 5;",
        "window.alert(a + 3);"
    ],
    id: "operations-mathadd"
});
makeTextArea({
    code: [
        "// Subtraction",
        "var a = 5;",
        "window.alert(a - 3);"
    ],
    id: "operations-mathsub"
});
makeTextArea({
    code: [
        "// Multiplication",
        "var a = 5;",
        "window.alert(a * 3);"
    ],
    id: "operations-mathmult"
});
makeTextArea({
    code: [
        "// Division",
        "var a = 5;",
        "window.alert(a / 3);"
    ],
    id: "operations-mathdiv"
});
makeTextArea({
    code: [
        "// Modulus (division remainder)",
        "var a = 5;",
        "window.alert(a % 3);"
    ],
    id: "operations-mathmodulus"
});
</script>

##### Assignment
* The **assignment operator** looks like an equals sign. It takes the value of whatever is on the right side and writes it into to the variable on the left side.
<script>
makeTextArea({
    code: [
        "// Assignment",
        "var a, b;",
        "b = 6;      // writes the value 6 into the variable b.",
        "a = 3 + b;  // writes the value 9 into the variable a.",
        "window.alert(a);"
    ],
    id: "operations-mathassign1"
});
</script>
* In JavaScript, the assignment operator looks like the equals sign from math, but they are not the same thing.
    * Equals in math is a statement of fact: "_this_ equals _that_".
    * Assignment in computing is an operation: "Put the value of whatever is on the right into the variable on the left."

##### Variables on both sides of the assignment operator
* One of the more useful things you can do with assignment is to put the same variable on both the left and right side of the assignment operator.
<script>
makeTextArea({
    code: [
        "// Variable on both sides",
        "var a = 0;",
        "a = a + 1;  // computes the value 0 + 1 and writes it into a.",
        "window.alert(a);"   
    ],
    id: "operations-mathassignbothsides1"
});
makeTextArea({
    code: [
        "// Variable on both sides",
        "var numApples = 5,",
        "    eatenApples = 2;",
        "numApples = numApples - eatenApples;",
        "window.alert(numApples);"    
    ],
    id: "operations-mathassignbothsides2"
});
</script>

##### Assignment operators
* For each basic arithmetic operator in JavaScript there is a corresponding **assignment operator**.
    * `a += b` same as `a = a + b`
    * `a -= b` same as `a = a - b`
    * `a *= b` same as `a = a * b`
    * `a /= b` same as `a = a / b`
    * `a %= b` same as `a = a % b`
* Example:
<script>
makeTextArea({
    code: [
        "// Assignment operator",
        "var a = 0;",
        "a += 100;  // computes the value a + 100 and writes it into a.",
        "window.alert(a);"    
    ],
    id: "operations-mathassignmentop"
});
</script>

##### Increment/decrement operators
* Operations of the form `a += 1` ("make `a` bigger by one") and `a -= 1` ("make `a` smaller by one") are so common that there are special operators just for this:
<script>
makeTextArea({
    code: [
        "// Increment operator",
        "var a = 100;",
        "a++;    // add 1 to a",
        "window.alert(a);",
        "",
        "// Decrement operator",
        "var b = 10;",
        "b--;    // subtract 1 from b",
        "window.alert(b);"
    ],
    id: "operations-mathincrementdecrement"
});
</script>

##### Precedence
* You can combine several operations in one statement:
<script>
makeTextArea({
    code: [
        "var a;",
        "a = 2 + 5 * 6 / 3 - 1",
        "window.alert(a);"
    ],
    id: "operations-mathprecedence1"
});
</script>
* In situations like this, the order of operator precedence is the same as in math:
    * Multiplication and division before addition and subtraction.
    * Left before right.
* Also:
    * Modulus is at the same level as multiplication and division.
    * Assignment has the lowest precedence.
* You can use parenthesis to change the order of operations:
<script>
makeTextArea({
    code: [
        "var a;",
        "a = (2 + 5) * 6 / (3 - 1)",
        "window.alert(a);"
    ],
    id: "operations-mathprecedence2"
});
</script>

#### Comparison operations
* **Comparison operators** (also called relational operators) are used to determine relative sizes of values. Their result is a Boolean value.
<script>
makeTextArea({
    code: [
        "var a = 2,",
        "    b = 1000;",
        "window.alert(a > b);     // greater than",
        "window.alert(a < b);     // less than ",
        "window.alert(a >= b);    // greater than or equal to",
        "window.alert(a <= b);    // less than or equal to"
    ],
    id: "operations-comparison1"
});
</script>
* JavaScript has two different concepts of "the same":
    * Exactly equal `===` (same type and value)
    * Effectively equal `==` (same value after any possible type changes have been made).
* Most of the time you will want to use `===` (exactly equal).
<script>
makeTextArea({
    code: [
        "var a = 5,",
        "    b = '5';",
        "window.alert(a === b);   // exactly equal (type and value must be the same)",
        "window.alert(a == b);    // effectively equal (value same after conversion)"
    ],
    id: "operations-comparison2"
});
</script>
* Similarly, JavaScript has two different concepts of "different":
    * Exactly not equal `!==` (different type and/or value)
    * Effectively not equal `!=` (different value after any possible type changes have been made).
* Most of the time you will want to use `!==` (exactly not equal).
<script>
makeTextArea({
    code: [
        "var a = 5,",
        "    b = '5';",
        "window.alert(a !== b);   // exactly not equal (type and/or value different)",
        "window.alert(a != b);    // effectively not equal (value different)"
    ],
    id: "operations-comparison3"
});
</script>
* The comparison operators have lower precedence than the arithmetic operators.
<script>
makeTextArea({
    code: [
        "var a = 2,",
        "    b = 1000;",
        "",
        "window.alert(a > (b - 999) );",
        "// ... can be written ...",
        "window.alert(a > b - 999);",

    ],
    id: "operations-comparison4"
});
</script>

#### Logical operations
* For complex control of logic, we will need **logical operators**---which take one or more Boolean values as operands and produce a Boolean result.
    * The usefulness of logical operations will make much more sense once we start to learn **control flow**.
* An **AND** (`&&`) operation will produce a true result only if _both_ its operands are true:
<script>
makeTextArea({
    code: [
        "var a = 1,",
        "    b = 100,",
        "    c = 1000;",
        "",
        "window.alert( (a > b) && (a > c) );   // false && false -> false",
        "window.alert( (a > b) && (a < c) );   // false && true  -> false",
        "window.alert( (a < b) && (a > c) );   // true  && false -> false",
        "window.alert( (a < b) && (a < c) );   // true  && true  -> true"
    ],
    id: "operations-logicaland"
});
</script>
* An **OR** (`||`) operation will produce a true result if _either_ or _both_ its operands are true:
<script>
makeTextArea({
    code: [
        "var a = 1,",
        "    b = 100,",
        "    c = 1000;",
        "",
        "window.alert( (a > b) || (a > c) );   // false || false -> false",
        "window.alert( (a > b) || (a < c) );   // false || true  -> true",
        "window.alert( (a < b) || (a > c) );   //  true || false -> true",
        "window.alert( (a < b) || (a < c) );   //  true || true  -> true"
    ],
    id: "operations-logicalor"
});
</script>
* A **NOT** (`!`) operation takes one operand and produces the operand's opposite value:
<script>
makeTextArea({
    code: [
        "var a = 1;",
        "",
        "window.alert( !(a > 0) );   // !true  -> false",
        "window.alert( !(a < 0) );   // !false -> true"
    ],
    id: "operations-logicalnot"
});
</script>

#### Operations on strings
* The `+` operator when applied to strings **concatenates** them (i.e., puts them together).
<script>
makeTextArea({
    code: [
        "var name, theMsg;",
        "",
        "name = 'Mithat';",
        "theMsg = 'My name is ' + name;",
        "window.alert(theMsg);"
    ],
    id: "operations-stringconcat1"
});
makeTextArea({
    code: [
        "var name, theMsg;",
        "",
        "name = window.prompt('What is your name?');",
        "theMsg = 'Hello, ' + name + '. Nice to meet you!';",
        "window.alert(theMsg);"
    ],
    id: "operations-stringconcat2"
});
</script>

* Comparison operators also work with strings.
<script>
makeTextArea({
    code: [
        "var str1 = 'aaaaa'",
        "    str2 = 'zzzzz'",
        "",
        "window.alert(str1 < str2);",
        "window.alert(str1 === str2);"
    ],
    id: "operations-stringcompare1"
});
</script>

#### Mixing strings with other types
* When using the `+` operator, if one of the operands is a string, then the other operand will be converted to a string as well.
<script>
makeTextArea({
    code: [
        "var theMsg, age;",
        "",
        "theMsg = 'Please vote if you are at least this age: ';  // a string",
        "age = 18;                                               // a number ",
        "",
        "window.alert(theMsg + age);"
    ],
    id: "operations-stringconcat3"
});
makeTextArea({
    code: [
        "var theQuestion, numMonsters, resp;",
        "",
        "numMonsters = 7;",
        "theQuestion = 'Destroy ' + numMonsters + ' monsters?';",
        "",
        "resp = window.confirm(theQuestion);"
    ],
    id: "operations-stringconcat4"
});
makeTextArea({
    code: [
        "var a = 5,", 
        "    b = 100;",
        "",
        "window.alert('a > b is ' + (a > b));"
    ],
    id: "operations-stringconcat5"
});
</script>
* Watch out for the order of operations!
<script>
makeTextArea({
    code: [
        "var a = 5,", 
        "    b = 10;",
        "",
        "window.alert(a + b + 'foobar');      // 15foobar",
        "window.alert('foobar' + a + b);      // foobar510",
        "window.alert('' + a + b +'foobar');  // 510foobar"
    ],
    id: "operations-stringconcat6"
});
</script>

### Control flow
* **Control flow** means, "Things you can do to affect the order in which things happen."
* JavaScript (like a lot of languages) provides three mechanisms for control flow:
    * Sequence
    * Selection
    * Repetition

#### Sequence
* **Sequence** means statements are executed one after another.
* Sequence happens by default in JavaScript (and a lot of other languages).
<script>
makeTextArea({
    code: [
        "var a = 2;                  // this happens first...",
        "window.alert('One');        // then this...",
        "a = 3;                      // then this...",
        "window.alert('Two');        // then this...",
        "window.alert(a);            // then this...",
        "window.alert('Squirrel!');  // then this..."
    ], 
    id: "sequence-01"
});
</script>
* There's not much more to say about that.

#### Selection
* A lot of programming logic has to deal with making decisions.
* **Selection structures** let you implement decisions by selectively execute blocks of statements based on some kind of condition(s).
* We will look at three of JavaScript's selections structures: _if_, _if/else_, and _nested if/else_.

##### if
* One of the most fundamental kinds of decisions you can make is: _if some condition is true, then do some action_.
    * _If_ I am dirty, _then_ take a bath.
    * _If_ it is dinner time, _then_ make dinner, set the table, and eat.
* In JavaScript, this kind of logic is handled with the **`if`** statement.
<script>
makeTextArea({
    code: [
        "var numMonsters;",
        "",
        "numMonsters = 0;",
        "",
        "if (numMonsters === 0)",
        "    window.alert('You have destroyed all monsters!');",
        "",
        "window.alert('You have the power!!!');"
    ], 
    id: "selection-if-1"
});
</script>

* If you need to execute more than one statement when the condition is true, then wrap the set of statements inside **curly brackets**:
<script>
makeTextArea({
    code: [
        "var today = 'thursday';",
        "",
        "if (today === 'friday') {",
        "    window.alert('Yay!');",
        "    window.alert('I get to sleep in tomorrow!');",
        "}",
        "",
        "window.alert(\"Tomorrow, tomorrow, there's always tomorrow.\");"

    ], 
    id: "selection-if-2"
});
</script>

* Note that you don't need a semicolon after the closing curly bracket, because the closing curly bracket pretty much says, "the show ends here."

##### if/else
* A lot of times, you will want to do one thing if something is true and something else if it is false. One way to do this is:
<script>
makeTextArea({
    code: [
        "var numMonsters;",
        "",
        "numMonsters = 0;",
        "",
        "if (numMonsters === 0) {",
        "    window.alert('You have destroyed all monsters!');",
        "}",
        "if (numMonsters !== 0) {",
        "    window.alert('Must eliminate more monsters!');",
        "}",
        "",
        "window.alert('You have the power!!!');"
    ], 
    id: "selection-ifelse-1"
});
</script>

* A much better way is to use JavaScript's **if/else** structure:
<script>
makeTextArea({
    code: [
        "var numMonsters;",
        "",
        "numMonsters = 0;",
        "",
        "if (numMonsters === 0) {",
        "    window.alert('You have destroyed all monsters!');",
        "}",
        "else {",
        "    window.alert('Must eliminate more monsters!');",
        "}",
        "",
        "window.alert('You have the power!!!');"
    ], 
    id: "selection-ifelse-2"
});
</script>

* It's better because:
    * You only have to write the test condition once (saves typing, less liklihood of a mistake).
    * The program only has to run one test---which makes things run faster.

##### nested if/else
* You can test for multiple conditions by putting if/else statements inside each other. This is called a **nested if/else**:
<script>
makeTextArea({
    code: [
        "var numMonsters, msg;",
        "",
        "numMonsters = 0;",
        "",
        "if (numMonsters === 0) {",
        "    msg = 'You have destroyed all monsters!';",
        "}",
        "else if (numMonsters === 1) {",
        "    msg = 'One to go!';",
        "}",
        "else if (numMonsters === 2) {",
        "    msg = 'Only a couple left!';",
        "}",
        "else if (numMonsters < 5) {",
        "    msg = 'Only a few left!';",
        "}",
        "else {",
        "    msg = 'Work harder!!!';",
        "}",
        "",
        "window.alert(msg + '\\nYou have the power!!!');"
    ], 
    id: "selection-nestedifelse-1"
});
</script>


#### First "game": number guessing
* And now we can _finally_ write a simple game. It's lame, but it's a game (arguably), and it's complete.
<script>
makeTextArea({
    code: [
        "// A simple guessing game.",
        "var myNum = 21,",
        "    yourNum = null;",
        "",
        "yourNum = window.prompt('I am thinking of a number from one to one hundred.\\nCan you guess what it is?');",
        "",
        "// In the comparison below, we need to use \"effectively equal\" to convert the string yourNum to a number.",
        "if (myNum == yourNum) {",
        "  window.alert('Congratuations!');",
        "}",
        "else {",
        "  window.alert('Fail.\\nAll your base are belong to us.');",
        "}"
    ], 
    id: "guessing-game-01"
});
</script>
* While the above works, it's generally better to use JavaScript's "exactly equals" operator.
* We can do this if we use JavaScript's built-in [`Number()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number) constructor to make a number from the user input before making the comparison: 
<script>
makeTextArea({
    code: [
        "// A simple guessing game (improved).",
        "var myNum = 21,",
        "    yourNum = null;",
        "",
        "yourNum = window.prompt('I am thinking of a number from one to one hundred.\\nCan you guess what it is?');",
        "",
        "// In the comparison below, we use Number() make a number from the user input and then use \"exactly equals\".",
        "if (myNum === Number(yourNum)) {",
        "  window.alert('Congratuations!');",
        "}",
        "else {",
        "  window.alert('Fail.\\nAll your base are belong to us.');",
        "}"
    ], 
    id: "guessing-game-01a"
});
</script>

#### Repetition

* Another pattern that comes up a lot is repeating something over and over again:
    * _while there is coffee left in my cup, take a sip of coffee._
    * _For each item on my shopping list, find it in the store and put it in my basket._
* This is called **repetition** or **looping** in programming.
* JavaScript has a several structures for doing this.

##### while

* The `while` structure is a direct expression of the idea: _while something is true, do some stuff_.
<script>
makeTextArea({
    code: [
        "/* Print integers from 1 to 100 to console. */",
        "var i = 1;",
        "",
        "while (i <= 100) {",
        "    console.log(i);",
        "    i++;",
        "}"
    ],
    id: "while-counter-1"
});
makeTextArea({
    code: [
        "/* Compute the sum of integers from 1 to 100. */",
        "var i = 1,",
        "    sum = 0;",
        "",
        "while (i <= 100) {",
        "    sum += i;",
        "    i++;",
        "}",
        "",
        "window.alert('The sum of integers 1 to 100 is: ' + sum);"
    ],
    id: "while-counter-2"
});
makeTextArea({
    code: [
"/* Compute the average of five user-entered values.  */",
"var i = 1,",
"    sum = 0,",
"    NUM_VALUES = 5,",
"    num;",
"",
"while (i <= NUM_VALUES) {",
"    num = window.prompt('Enter a number: ');",
"    num = Number(num);",
"    sum += num;",
"    i++;",
"}",
"",
"window.alert('The average of those numbers is: ' + sum/NUM_VALUES);"
    ],
    id: "while-counter-3"
});
</script>

* The preceding were examples of repetition using a **counter** (the variable `i` in the examples).
* You can actually do repetition based on _any_ kind of condition, including a condition that the user specifies:
<script>
makeTextArea({
    code: [
        "/* Parrot back back whatever the user enters. */",
        "var something = '';",
        "",
        "while (something !== 'done') {",
        "    something = window.prompt('Tell me something (or enter \\'done\\' to end): ');",
        "    window.alert('You said: ' + something);",
        "}"
    ],
    id: "while-semaphore-1"
});
</script>

* The biggest danger with `while` loops is that you can create a situation where the repetition never ends. This is called an **infinite loop**.
<script>
makeTextArea({
    code: [
"/* Example of an infinite loop. */",
"var i = 1,",
"    sum = 0;",
"",
"// i stays at 1 forever, so the condition is never false.",
"while (i <= 100) {",
"    sum += i;",
"}",
"",
"window.alert('The sum of numbers 1 to 100 is: ' + sum);"
    ],
    id: "while-infinite-1"
});
</script>

##### Improving the number guessing game
* Here is an improved version of the guessing game from earlier that lets the user continue to guess until she's got it right:
<script>
makeTextArea({
    code: [
"// A simple guessing game (more improved).",
"var myNum = 21,",
"    isGuessCorrect = false,",
"    yourNum = null;",
"",
"while (isGuessCorrect === false) {",
"    yourNum = window.prompt('I am thinking of a number from one to one hundred.\\nCan you guess what it is?');",
"    ",
"    if (myNum === Number(yourNum)) {",
"      window.alert('Congratuations!');",
"      isGuessCorrect = true;",
"    }",
"    else {",
"      window.alert('Fail. Try again.');",
"      isGuessCorrect = false;",
"    }",
"}"
    ],
    id: "guessing-game-1b"
});
</script>
* We can tighten this up even more:
<script>
makeTextArea({
    code: [
"// A simple guessing game (slightly more improved).",
"var myNum = 21,",
"    isGuessCorrect = false,",
"    yourNum = null;",
"",
"while (!isGuessCorrect) {",
"    yourNum = window.prompt('I am thinking of a number from one to one hundred.\\nCan you guess what it is?');",
"    ",
"    if (myNum === Number(yourNum)) {",
"      window.alert('Congratuations!');",
"      isGuessCorrect = true;",
"    }",
"    else {",
"      window.alert('Fail. Try again.');",
"    }",
"}"
    ],
    id: "guessing-game-1c"
});
</script>

##### do...while
* The **`do...while`** structure is similar to the `while` structure except that the continuation test happens at the _end_ rather than at the _beginning_.
<script>
makeTextArea({
    code: [
"/* Parrot back back whatever the user enters. */",
"var something;",
"",
"do {",
"    something = window.prompt('Tell me something (or enter \\'done\\' to end): ');",
"    window.alert('You said: ' + something);",
"} while (something !== 'done' && something !== null);"
    ],
    id: "do-while-semaphore-1"
});
</script>

##### counter-controlled for
* Repeating something based on a counter typically follows the same pattern:
<script>
makeTextArea({
    code: [
"/* Compute the sum of integers from 1 to 100. */",
"var sum = 0,",
"    i = 1;          // 1. initialize a counter variable",
"",
"while (i <= 100) {  // 2. test the counting condition",
"    sum += i;       // 3. do some stuff",
"    i++;            // 4. increment the counter",
"}",
"",
"window.alert('The sum of integers 1 to 100 is: ' + sum);"
    ],
    id: "while-counter-2-annotated"
});
</script>
* JavaScript's **counter-controlled for** puts all of this into a single structure.
* This makes counter-controlled repetition easier to write and less likely to have bugs.
* Counter-controlled for looks like:
    for (<initialize-counter>; <test-condition>; <increment>) {
        <the-stuff-you-want-to-do>
    }
* Example:
<script>
makeTextArea({
    code: [
"/* Compute the sum of integers from 1 to 100. */",
"var i,",
"    sum = 0;",
"",
"for (i=1; i<=100; i++) {",
"    sum += i;",
"}",
"",
"window.alert('The sum of integers 1 to 100 is: ' + sum);"
    ],
    id: "for-counter-2"
});
</script>

* You can place the counter variable declaration in the structure as well:
<script>
makeTextArea({
    code: [
"/* Compute the sum of integers from 1 to 100. */",
"var sum = 0;",
"",
"for (var i = 1; i <= 100; i++) {",
"    sum += i;",
"}",
"",
"window.alert('The sum of integers 1 to 100 is: ' + sum);"
    ],
    id: "for-counter-2a"
});
</script>

* Here is the averaging program written using counter-controlled for:
<script>
makeTextArea({
    code: [
"/* Compute the average of five user-entered values.  */",
"var sum = 0,",
"    NUM_VALUES = 5;",
"",
"for (var i = 1; i <= NUM_VALUES; i++) {",
"    num = window.prompt('Enter a number: ');",
"    num = Number(num);",
"    sum += num;",
"}",
"",
"window.alert('The average of those numbers is: ' + sum/NUM_VALUES);"
    ],
    id: "for-counter-3"
});
</script>

##### `break` and `continue`
* The **`break`** statement will get you completely out of a repetition structure early:
<script>
makeTextArea({
    code: [
"/* Print out the first integer between a lower and an upper bound that is",
" * divisible by 7. */",
"var i,",
"    msg = 'None found.',",
"    LOWER = 347,",
"    UPPER = 919;",
"",
"for (i = LOWER; i <= UPPER; i++) {",
"    if (i % 7 === 0) {",
"        msg = 'Found it! ' + i;",
"        break;",
"    }",
"}",
"",
"window.alert(msg);"
    ],
    id: "break-1"
});
</script>

* The **``continue``** statement will skip the rest of the body of a loop but continue the repetition:
<script>
makeTextArea({
    code: [
"/* Print out a list of numbers from 1 to 10 that are not divisible by 3. */",
"var i = 1,",
"    msg = '';",
"",
"for (i = 1; i <= 10; i++) {",
"    if (i % 3 === 0) {",
"        continue;",
"    }",
"    msg += i + ' ';",
"}",
"",
"window.alert(msg);"
    ],
    id: "continue-1"
});
</script>

<script>
window.onload = function () {
    activeNavLink('nav03');
}
</script>

* You don't want to use `break` and `continue` a lot, but they can be very useful in some situations.

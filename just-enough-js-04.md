% Just Enough JavaScript / 4 /
% Mithat Konar
%

Functions
---------
* **Functions** in JavaScript are based on the concept of functions in mathematics:
    * Given: _f(x) = 3x + 1_, <br> _f(2)_ would return the value _7_, _f(11)_ would return the value _34_ ...
    * _f_ is the **name of the function**
    * _x_ is a **parameter**

### Programmer-defined functions
* JavaScript lets you define your own functions. We might write a definition for the function above as follows:
<script>
makeTextArea({
    code: [
"function f(x) {",
"    var val;",
"    val = 3*x + 1;",
"    return val;",
"}"
    ],
    id: "function-1"
});
</script>

* The definition won't do anything unless we **call** (or **invoke**) it:
<script>
makeTextArea({
    code: [
"function f(x) {",
"    var val;",
"    val = 3*x + 1;",
"    return val;",
"}",
"",
"window.alert('f(2) is ' + f(2));",
"window.alert('f(11) is ' + f(11));"
    ],
    id: "function-1a"
});
</script>

* As was the case with variable names, you typically want to use descriptive names for functions.
* Name rules for functions are the same as for variables.
<script>
makeTextArea({
    code: [
"/* myLine(x) computes the y value for my line given an x value. */",
"function myLine(x) {",
"    var val;        // create a local variable to compute stuff with",
"    val = 3*x + 1;  // calculate stuff",
"    return val;     // return the calculated value",
"}",
"",
"var y;",
"",
"y = myLine(2);",
"window.alert( 'The y value for my line when x is 2 is ' + y );",
"",
"y = myLine(11);",
"window.alert( 'The y value for my line when x is 11 is ' + y );"
    ],
    id: "function-1b"
});
</script>

* When you call (or invoke) a function, you can pass in literal values or variables.
<script>
makeTextArea({
    code: [
"/* myLine(x) computes the y value for my line given an x value. */",
"function myLine(x) {",
"    var val;        // create a local variable to compute stuff with",
"    val = 3*x + 1;  // calculate stuff",
"    return val;     // return the calculated value",
"}",
"",
"var x, y;",
"x = 2;",
"y = myLine(x);",
"window.alert( 'The y value for my line when x is ' + x + ' is ' + y );"
    ],
    id: "function-1c"
});
</script>

* You can define as many functions as you need and have functions call other functions.
<script>
makeTextArea({
    code: [
"function addThreeNumbers(a, b, c) {",
"    var total = a + b + c;",
"    return total;",
"}",
"",
"function averageThreeNumbers(x, y, z) {",
"    var average = addThreeNumbers(y, z, x) / 3;",
"    return average;",
"}",
"",
"// Test the functions:",
"var msg, tot, avg;",
"",
"tot = addThreeNumbers(6, 7, 9);",
"avg = averageThreeNumbers(6, 7, 9);",
"",
"msg = 'The total is: ' + tot + ",
"      '\\nThe average is: ' + avg;",
"window.alert(msg);"
    ],
    id: "function-X"
});
</script>

* You can define a function _inside_ another function---but we'll not deal with that now.
* Functions can be simple as above or more complicated:
<script>
makeTextArea({
    code: [
"/* weeklyPay computes the weekly pay for someone given their total hours worked. */",
"function weeklyPay(numHours) {",
"    var HOURLY_WAGE = 10.00,",
"        OVERTIME_FACTOR = 1.5,",
"        basepay = 0,",
"        overtime = 0;",
"    ",
"    if (numHours <= 40) {",
"        basepay = HOURLY_WAGE * numHours;",
"    }",
"    else {",
"        // overtime!",
"        basepay = HOURLY_WAGE * 40;",
"        overtime = HOURLY_WAGE * OVERTIME_FACTOR * (numHours-40);",
"    }",
"    ",
"    return basepay + overtime;",
"}",
"",
"var hours, myPay;",
"",
"hours = window.prompt('How many hours did you work this week?');",
"hours = Number(hours);",
"myPay = weeklyPay(hours);",
"",
"window.alert('Week\\'s pay: ' + myPay);"
    ],
    id: "function-2"
});
</script>

* You can pass in several parameters:
<script>
makeTextArea({
    code: [
"/* weeklyPay computes the weekly pay for someone given their hourly wage and",
" * their total hours worked. */",
"function weeklyPay(numHours, hourlyWage) {",
"    var OVERTIME_FACTOR = 1.5,",
"        basepay = 0,",
"        overtime = 0;",
"    ",
"    if (numHours <= 40) {",
"        basepay = hourlyWage * numHours;",
"    }",
"    else {",
"        // overtime!",
"        basepay = hourlyWage * 40;",
"        overtime = hourlyWage * OVERTIME_FACTOR * (numHours-40);",
"    }",
"    ",
"    return basepay + overtime;",
"}",
"",
"var wage, hours, myPay;",
"",
"wage = window.prompt('What is your hourly wage?');",
"hours = window.prompt('How many hours did you work this week?');",
"wage = Number(wage);",
"hours = Number(hours);",
"myPay = weeklyPay(hours, wage);",
"",
"window.alert('Week\\'s pay: ' + myPay);"
    ],
    id: "function-3"
});
</script>

### Beyond math

* Any data type (not just numbers!) can be used for parameters or the return value.
<script>
makeTextArea({
    code: [
"/* formatNameWesternFormal returns a string representing the full",
" * name of an individual in formal Western format. */",
"function formatNameWesternFormal(givenName, familyName) {",
"    var fullName;",
"    ",
"    fullName = familyName + ', ' + givenName;",
"    return fullName;",
"}",
"",
"// Test formatNameWesternFormal()",
"var nom;",
"",
"nom = formatNameWesternFormal('Kareena', 'Kapoor');",
"window.alert(nom);"
    ],
    id: "fcn-data-types-1"
});
</script>

* You don't need parameters:
<script>
makeTextArea({
    code: [
"function getFavoriteColor() {",
"    var color;",
"    ",
"    color = window.prompt('What is your favorite color?');",
"    return color;",
"}",
"",
"// Test getFavoriteColor()",
"var yourColor;",
"",
"yourColor = getFavoriteColor();",
"",
"window.alert('I like ' + yourColor + ' too!');"
    ],
    id: "fcn-no-params-1"
});
</script>

* You don't even need a return value:
<script>
makeTextArea({
    code: [
"function showVoterMsg(name, age) {",
"    var msg;",
"    ",
"    msg = 'Hello, ' + name + '.';",
"    ",
"    if (age >= 18) {",
"        msg += ' The voting booth is to the right.';",
"    }",
"    else {",
"        msg += ' You can vote when you are older.';",
"    }",
"    ",
"    window.alert(msg);",
"}",
"",
"// Test showVoterMsg()",
"showVoterMsg('John Plummer', 17);",
"showVoterMsg('Ofra Zurer', 18);"
    ],
    id: "fcn-no-return-val-1"
});
</script>

### Predefined funtions
* In addition to functions you write yourself, JavaScript has a large list of predefined functions.
* Many of these are defined in JavaScript's [`Math`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math) object. 
    * These are technically _methods_, but we will let the distinction slide for now.
* Some useful functions in the `Math` object are summarized below:
    * [`Math.round(x)`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round) --- Returns the value of a number rounded to the nearest integer.
    *  [`Math.floor(x)`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/floor) --- Returns the largest integer less than or equal to a number.
    *  [`Math.ceil(x)`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/ceil) --- Returns the smallest integer greater than or equal to a number.
* You should take some time to familiarize yourself with the others.

### Random numbers
* One of the most useful things in the `Math` object for us is [`Math.random()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random).
* `Math.random()` returns a different random number between 0 (inclusive) and 1 (exclusive) each time it is called.
<script>
makeTextArea({
    code: [
"var val;",
"",
"for (var i = 1; i <= 10; i++) {",
"    val = Math.random();    // Generate a random value from 0 to 0.9999...",
"    console.log(val);       // Remember to open the console!",
"}"
    ],
    id: "random-int-1"
});
</script>

* We can use this as a  basis for creating our own function that returns a random integer between a lower and upper bound.
<script>
makeTextArea({
    code: [
"/* randomInt returns a random integer between lower and upper (inclusive). */",
"function randomInt(lower, upper) {",
"    var range = upper-lower;",
"    return Math.floor(Math.random() * (range+1) + lower);",
"}",
"",
"// Test randomInt().",
"window.alert(randomInt(1, 10));"
    ],
    id: "random-int-2"
});
</script>

### A real guessing game
* Armed with the `randomInt()` function defined above, we can now modify our guessing game so the user needs to guess a different number each time he plays.
<script>
makeTextArea({
    code: [
"/* A simple guessing game (more++ improved). */",
"",
"/* randomInt returns a random integer between lower and upper (inclusive). */",
"function randomInt(lower, upper) {",
"    var range = upper-lower;",
"    return Math.floor(Math.random() * (range+1) + lower);",
"}",
"",
"var myNum = randomInt(1, 10),",
"    yourNum = null,",
"    isGuessCorrect = false;",
"",
"while (!isGuessCorrect) {",
"    yourNum = window.prompt('I am thinking of a number from one to ten.\\nCan you guess what it is?');",
"    ",
"    if (myNum === Number(yourNum)) {",
"      window.alert('Congratuations!');",
"      isGuessCorrect = true;",
"    }",
"    else {",
"      window.alert('Fail. Try again.');",
"    }",
"}"
    ],
    id: "guessing-game-2"
});
</script>

* Here we define an additional function to make getting the user's guess cleaner:
<script>
makeTextArea({
    code: [
"/* A simple guessing game (more++ improved alternate version). */",
"",
"/* randomInt returns a random integer between lower and upper (inclusive). */",
"function randomInt(lower, upper) {",
"    var range = upper-lower;",
"    return Math.floor(Math.random() * (range+1) + lower);",
"}",
"",
"/* getGuess gets a guess from the user and returns it as a number. */",
"function getGuess() {",
"    var guess = window.prompt('I am thinking of a number from one to ten.\\nCan you guess what it is?');",
"    return Number(guess);",
"}",
"",
"var myNum = randomInt(1, 10),",
"    yourNum = null,",
"    isGuessCorrect = false;",
"",
"while (!isGuessCorrect) {",
"    yourNum = getGuess();",
"    ",
"    if (myNum === yourNum) {",
"      window.alert('Congratuations!');",
"      isGuessCorrect = true;",
"    }",
"    else {",
"      window.alert('Fail. Try again.');",
"    }",
"}"
    ],
    id: "guessing-game-2b"
});
</script>

### Scope

* The following examples help to illustrate how **scope** works in JavaScript.
* Consider the following example where we define a variable inside a function with same name as one outside the function:
<script>
makeTextArea({
    code: [
"/* Scope example 1 */",
"",
"function myFunc() {",
"    var x;",
"    x = 99;",
"    console.log(x);",
"}",
"",
"var x;",
"x = 1;",
"console.log(x);    // Remember to open the console!",
"myFunc();",
"console.log(x);"
    ],
    id: "function-scope-1"
});
</script>

* And now with one small change---eliminating the variable declaration inside the function:
<script>
makeTextArea({
    code: [
"/* Scope example 2 */",
"",
"function myFunc() {",
"    x = 99;",
"    console.log(x);",
"}",
"",
"var x;",
"x = 1;",
"console.log(x);    // Remember to open the console!",
"myFunc();",
"console.log(x);"
    ],
    id: "function-scope-2"
});
</script>

* And another small change---removing the declaration outside the function:
<script>
makeTextArea({
    code: [
"/* Scope example 3 */",
"",
"function myFunc() {",
"    var x;",
"    x = 99;",
"    console.log(x);",
"}",
"",
"myFunc();",
"console.log(x);    // Remember to open the console!"
    ],
    id: "function-scope-3"
});
</script>

* Scope means "where a variable can be accessed."
* Variables can be **global** or **local**.
    * Variables declared outside a function are **global variables** and can be accessed anywhere.
    * Variables declared inside a function are **local variables**.
        * Local variables can only be accessed inside that function and things defined inside that function.
        * Parameters have the same scope as variables defined within the function.
* If a variable is declared inside a function with a name that is same as one in an encompassing scope, then the variable defined in the function is the one that is accessed with the name.

#### The importance of `var`
* If you use a name on the left side of an assignment that is not already the name of a variable that can be accessed, JavaScript will automatically create a new variable for you with that name.<br>
`foo = 'Some value';    // Creates a new global variable foo if foo doesn't already exist.`{.javascript}
* Variables created in this way are _automatically global variables_.
* Having all your names in the global namespace is a really bad idea---eventually you will accidentally think you are creating a new variable when in fact you are writing over a variable that is used someplace else.
* **Always, always, always use `var` unless you have a good reason not to**.

### Functions as "values"
* So far, we have seen numbers, strings, Booleans, and the special values `undefined` and `null` used as values for variables.
* In JavaScript, _functions_ can also be used as the value of a variable:
<script>
makeTextArea({
    code: [
"function doubleIt(num) {",
"    return (2 * num);",
"}",
"",
"function tripleIt(num) {",
"    return (3 * num);",
"}",
"",
"var myFunc = doubleIt;    // myFunc  points to function doubleIt",
"window.alert('4 times 2 is ' + myFunc(4));",
"",
"myFunc = tripleIt;       // myFunc now points to function tripleIt",
"window.alert('4 times 3 is ' + myFunc(4));"
    ],
    id: "fcn-val-1"
});
</script>

* Here is a (slightly) more useful example:
<script>
makeTextArea({
    code: [
"/* Ask the user her age and what she would like to do (drive, vote, ",
" * or drink), then tell user if her age qualifies her to do that.",
" */",
"",
"/* Return true iff someone at age can drive. */",
"function canDrive(age) {",
"    return (age >= 16);",
"}",
"",
"/* Return true iff someone at age can vote. */",
"function canVote(age) {",
"    return (age >= 18);",
"}",
"",
"/* Return true iff someone at age can drink alcohol. */",
"function canDrink(age) {",
"    return (age >= 21);",
"}",
"",
"/* Return whether someone at age can do qualFunc. */",
"function doesQualify(age, qualFunc) {",
"    var q = qualFunc(age);",
"    if ( qualFunc(age) ) {",
"        window.alert('You can do that!');",
"    }",
"    else {",
"        window.alert('Sorry. You don\\'t get to do that.');    ",
"    }",
"        ",
"    return q;",
"}",
"",
"// Get user's age",
"var years,      // the age of the user",
"    selection,  // the thing the user wants to do",
"    wantToDo;   // the function that will be used to see if user can do it ",
"",
"years = window.prompt('How old are you?');",
"years = Number(years);",
"selection = window.prompt('What do you want to do?\\n(Enter \"drive\", \"vote\", or \"drink\")');",
"",
"if (selection === 'drive') {",
"    wantToDo = canDrive;",
"}",
"else if (selection === 'vote') {",
"    wantToDo = canVote;",
"}",
"else if (selection === 'drink') {",
"    wantToDo = canDrink;",
"}",
"",
"doesQualify(years, wantToDo);"
    ],
    id: "fcn-val-2"
});
</script>

* At first, the utility of this might seem limited, but **this is a core JavaScript concept**.

### Function literals
* You don't need to provide a function a name if it is used as a value for a variable:
<script>
makeTextArea({
    code: [
"/* Yet another version of the guessing game ---this one showing assignment",
" * of an anonymous function to a variable. */",
"",
"var myNum,",
"    yourNum,",
"    getGuess;",
"",
"myNum = 99;",
"",
"// Set getGuess to be a function that gets a number guess from the user.",
"// The stuff on the right of the = operator creates an anonymous function;",
"// it gets assigned to the variable on the left side of the = operator.",
"getGuess = function (name) {",
"        var g;  // the guess",
"        g = window.prompt('Hello, ' + name + '. Guess a number: ');",
"        return Number(g);  // convert guess to a Number and return it.",
"    };",
"",
"// Get a guess from the user and compare.",
"yourNum = getGuess('Cleopatra');",
"",
"if (yourNum === myNum) {",
"    window.alert('Cheers!');",
"}",
"else {",
"    window.alert('Boo!');",
"}"
    ],
    id: "fcn-anon-1"
});
</script>

* Or a parameter:
<script>
makeTextArea({
    code: [
"/* function notifyUser notifies the user using function notifyFunc",
" * and makes a note in the log about the notification. */",
"function notifyUser(someFunc) {",
"    someFunc();",
"    console.log('The user has been notified');",
"}",
"",
"// Call notifyUser using a function literal for notifyFunc that",
"// notifies using window.alert.",
"notifyUser(function () {",
"    window.alert('Your hovercraft is full of eels.');",
"});",
"",
"// Call notifyUser using a function literal for notifyFunc that",
"// notifies using console.log.",
"notifyUser(function () {",
"    console.log('I doubt the user will see this notification.');",
"});"
    ],
    id: "fcn-anon-2"
});
</script>

* Function definitions without names are called **function literals**. You will see them a lot.

<script>
window.onload = function () {
    activeNavLink('nav04');
}
</script>

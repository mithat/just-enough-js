% Just Enough JavaScript / 5 /
% Mithat Konar
%

Arrays
------
* An **array** is a structure that lets you store a list of values.

### Basics
* You can create an array using an _array literal_: a pair of square brackets with a comma-separated list of values in between.
* Arrays can be assigned to variables.
<script>
makeTextArea({
    code: [
"var a = [2, 4, 8, 42];    // Creates an array with 4 numbers in it.",
"var b = [];               // Creates an empty array.",
"",
"console.log(a);",
"console.log(b);"
    ],
    id: "array-create-1"
});
</script>

* You can also create an array using the array constructor:
<script>
makeTextArea({
    code: [
"var yourArray = new Array(3);      // Creates an array of 3 undefined values.",
"console.log(yourArray);",
"",
"var myArray = new Array(1, 6, 12); // Creates an array of 3 numbers.",
"console.log(myArray);"
    ],
    id: "array-create-2"
});
</script>

* Each "thing" in an array is an **element**.
* Elements in an array can be of any type:
<script>
makeTextArea({
    code: [
"var osArray = ['Windows', 'MacOS', 'Linux', 'Other'];  // Array of strings.",
"",
"console.log(osArray);",
    ],
    id: "array-create-3"
});
makeTextArea({
    code: [
"var myArray = [2, 'hi', 'ho', 42];    // An array with 2 numbers and 2 strings.",
"",
"console.log(myArray);",
"",
"function myFunc() {",
"    return 'Yo!';",
"}",
"",
"var yourArray = [5, myFunc, myArray, 'phft!'];   // number, function, array, string",
"",
"console.log(yourArray);"
    ],
    id: "array-create-4"
});
</script>

* The typical way of accessing an element is by using square brackets:
<script>
makeTextArea({
    code: [
"var myArray = [2, 'hi', 'ho', 42];    // An array with 2 numbers and 2 strings.",
"",
"console.log(myArray[0]);     // Prints 2",
"console.log(myArray[1]);     // Prints 'hi'",
"console.log(myArray[2]);     // Prints 'ho'",
"console.log(myArray[3]);     // Prints 42",
"",
"myArray[2] = 'fantabulous';  // Changes 'ho' to 'fantabulous'",
"console.log(myArray[2]);     // Prints 'fantabulous'"
    ],
    id: "array-access-1"
});
</script>

* The number by which you refer to an element is the **index** or **subscript**.
* _Index numbering starts from **zero**, not 1._
* An array knows how many elements it has in it:
<script>
makeTextArea({
    code: [
"var myArray = [2, 'hi', 'ho', 42];    // An array with 2 numbers and 2 strings.",
"",
"console.log('Number of elements in the array: ' + myArray.length);  // 4 elements in array"
    ],
    id: "array-length-1"
});
</script>

* This makes it easy to move element by element through an array:
<script>
makeTextArea({
    code: [
"var myArray = [2, 'hi', 'ho', 42];    // An array with 2 numbers and 2 strings.",
"",
"// Print out contents of myArray.",
"for (var i = 0; i < myArray.length; i++) {",
"    console.log(myArray[i]);",
"}"
    ],
    id: "array-create-N"
});
</script>

### Adding and removing elements
* You can add elements to an array by assigning something to an element beyond the end of the array:
<script>
makeTextArea({
    code: [
"var notes = ['do', 're', 'mi'];",
"",
"notes[3] = 'fa';   // adds a new element to array",
"console.log('length: ' + notes.length);",
"",
"for (var i = 0; i < notes.length; i++) {",
"    console.log(i + ': ' + notes[i]);",
"}"
    ],
    id: "array-add-element-1"
});
</script>

* If you add something far beyond the end, extra elements are `undefined`:
<script>
makeTextArea({
    code: [
"var notes = ['do', 're', 'mi'];",
"",
"notes[5] = 'la';   // adds 3 elements; notes[3] and notes[4] will be undefined",
"",
"console.log('length: ' + notes.length);",
"",
"for (var i = 0; i < notes.length; i++) {",
"    console.log(i + ': ' + notes[i]);",
"}"
    ],
    id: "array-add-element-2"
});
</script>

* There are also array _methods_ (a kind of function, see **Objects** below) that let you add elements:
<script>
makeTextArea({
    code: [
"var notes = ['do', 're', 'mi'];",
"",
"notes.push('fa', 'so');   // add elements to the end of the array",
"",
"console.log('length: ' + notes.length);",
"",
"for (var i = 0; i < notes.length; i++) {",
"    console.log(i + ': ' + notes[i]);",
"}"
    ],
    id: "array-push"
});
makeTextArea({
    code: [
"var notes = ['do', 're', 'mi'];",
"",
"notes.unshift('la', 'ti');   // add elements to the start of the array",
"",
"console.log('length: ' + notes.length);",
"",
"for (var i = 0; i < notes.length; i++) {",
"    console.log(i + ': ' + notes[i]);",
"}"
    ],
    id: "array-unshift"
});
</script>

* There are corresponding array methods that let you remove elements:
<script>
makeTextArea({
    code: [
"var notes = ['do', 're', 'mi'];",
"var removed;",
"",
"removed = notes.pop();   // remove an element from the end",
"console.log('removed: ' + removed);",
"",
"console.log('length: ' + notes.length);",
"",
"for (var i = 0; i < notes.length; i++) {",
"    console.log(i + ': ' + notes[i]);",
"}"
    ],
    id: "array-pop"
});
makeTextArea({
    code: [
"var notes = ['do', 're', 'mi'];",
"var removed;",
"",
"removed = notes.shift();   // remove an element from the start",
"console.log('removed: ' + removed);",
"",
"console.log('length: ' + notes.length);",
"",
"for (var i = 0; i < notes.length; i++) {",
"    console.log(i + ': ' + notes[i]);",
"}"
    ],
    id: "array-shift"
});
</script>

### Additional methods
* There are lots of other [methods that you can use with arrays](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#Array_instances) that are worth looking over.

Objects
-------
* You can think of **objects** as being similar to arrays except that you use identifiers to refer to the different parts of the structure instead of a numeric index.

### Basics
* _Object literals_ are defined using curly brackets and a comma-separated list of `key:value` pairs.
* Each `key:value` pair in the structure is a **member**.
* You access the value of a member using **dot notation**: `<object-name>.<key>`
<script>
makeTextArea({
    code: [
"var days = {",
"    sunday: 'bluesy',",
"    monday: 'stressy',",
"    tuesday: 'poopy'",
"};                          // don't forget the semicolon at the end of the var statement",
"",
"window.alert(days.monday);  // access the member *monday* of object *days*",
"console.log(days);			 // members are unordered"
    ],
    id: "object-creation-1"
});
</script>
* Keys use the same name rules that apply to variables and functions.
* Values can be anything: numbers, strings, Booleans, arrays, functions, other objects ...
<script>
makeTextArea({
    code: [
"var meaningless = {",
"    genre: 'dada',",
"    secretNumber: 77,",
"    hasConsequences: false",
"};                          // don't forget the semicolon at the end of the var statement",
"",
"console.log(meaningless);",
"console.log(meaningless.genre);",
"console.log(meaningless.secretNumber);",
"console.log(meaningless.hasConsequences);"
    ],
    id: "object-creation-2"
});
</script>

* You can also create an object using the generic Object _constructor_ (see below) and then add members manually:
<script>
makeTextArea({
    code: [
"var days = new Object();    // create an empty object",
"",
"days.sunday = 'bluesy';",
"days.monday = 'stressy';",
"days.tuesday = 'poopy';",
"",
"window.alert(days.monday);",
"console.log(days);"
    ],
    id: "object-creation-4"
});
</script>

* This is the same as using an empty object literal, which is generally [thought to be better](http://jslinterrors.com/use-the-object-literal-notation/):
<script>
makeTextArea({
    code: [
"var days = {};             // create an empty object",
"",
"days.sunday = 'bluesy';",
"days.monday = 'stressy';",
"days.tuesday = 'poopy';",
"",
"window.alert(days.monday);",
"console.log(days);"
    ],
    id: "object-creation-3"
});
</script>

* You can add members on the fly no matter how you created the object:
<script>
makeTextArea({
    code: [
"var days = {",
"    sunday: 'bluesy',",
"    monday: 'stressy',",
"    tuesday: 'poopy'",
"};                          // don't forget the semicolon at the end of the var statement",
"",
"days.wednesday = 'humpy';",
"days.thursday = 'almost-the-weekendy';",
"days.friday = 'tgify';",
"days.saturday = 'weekendy';",
"",
"console.log(days);"
    ],
    id: "object-adding-1"
});
</script>

### Properties and methods
* Objects are often used used to create a structure that describes something in terms of **properties**.
<script>
makeTextArea({
    code: [
"var bouncyBall = {",
"    size: 4,",
"    material: 'rubber',",
"    isHollow: true",
"};",
"",
"var squishyBall = {",
"    size: 3,",
"    material: 'foam',",
"    isHollow: false",
"};",
"",
"var myDog = {",
"    name: 'Fido',",
"    age: 3,",
"    breed: 'mutt',",
"    favoriteToy: bouncyBall",
"};",
"",
"var yourDog = {",
"    name: 'Rover',",
"    age: 5,",
"    breed: 'greyhoud',",
"    favoriteToy: myDog",
"};",
"",
"window.alert(myDog.name + ' is ' + myDog.age + ' years old and is a ' + myDog.breed + '.');",
"window.alert(yourDog.name + ' is ' + yourDog.age + ' years old and is a ' + yourDog.breed + '.');"
    ],
    id: "object-"
});
</script>

* You can attach functions to an object as well. A function attached to an object is a **method**. The functions are usually defined with function literals.
<script>
makeTextArea({
    code: [
"// Adding a speak method to myDog.",
"var myDog = {",
"    name: 'Fido',",
"    age: 3,",
"    speak: function () {",
"        window.alert('Woof!');",
"    },",
"    breed: 'mutt'",
"};",
"",
"myDog.speak();"
    ],
    id: "array-"
});
</script>

* Within a method, you can use the `this` keyword to refer to a member in the object.
<script>
makeTextArea({
    code: [
"var age = 99;",
"",
"// myDog knows how old he is.",
"var myDog = {",
"    name: 'Fido',",
"    age: 3,",
"    speak: function () {",
"        window.alert('Woof!');",
"    },",
"    howOld: function () {",
"        window.alert(this.age);	// try without 'this'",
"    },",
"    breed: 'mutt'",
"};",
"",
"myDog.howOld();",
"myDog.age += 1;		// Happy Birthday!",
"myDog.howOld();"
    ],
    id: "object-encapsulation-1"
});
makeTextArea({
    code: [
"var age = 99;",
"",
"// myDog will now bark his age if you ask him.",
"var myDog = {",
"    name: 'Fido',",
"    age: 3,",
"    speak: function () {",
"        window.alert('Woof!');",
"    },",
"    howOld: function () {",
"        var msg = '';",
"        for (var i = 1; i <= this.age; i++) {	// try without 'this'",
"            msg += 'Woof! ';",
"        }",
"        window.alert(msg);",
"    },",
"    breed: 'mutt'",
"};",
"",
"myDog.howOld();",
"myDog.age += 1;		// Happy Birthday!",
"myDog.howOld();"
    ],
    id: "object-encapsulation-2"
});
</script>
* Being able to define a structures that encapsulate a thing's state (properties) and behavior (methods) is incredibly useful.

### An object-oriented game
* Below is a game built using basic object-oriented principles.
* We define the `requisiteWarrior` entity using an object. The `requisiteWarrior` object bundles all the essential properties and behaviors relevant to the `requisiteWarrior` into one package.
* Gameplay is simple loop that is easy to read thanks to the `requisiteWarrior` object.
* The game is again quite lame. No skill, no strategy. Just pure chance. But it shows how objects can simplify writing the game.
<script>
makeTextArea({
    code: [
"// Define entities.",
"",
"/* requisiteWarrior starts out with a health of 20.",
" * When she does battle, there is a 50/50 chance that she will emerge",
" * victorious. Victory ramps up her health by 8, defeat down by 10.",
" * The requisiteWarrior dies when her health drops below 0. */",
"var requisiteWarrior = {",
"    health: 20,",
"",
"    /* doBattle makes requisiteWarrior do battle. If she wins, her health",
"     * is increased; if she looses it is reduced. ",
"     * Returns whether the battle resulted in victory or not. */",
"    doBattle: function () {",
"        var isVictorious = Math.random() > 0.5;",
"        if (isVictorious) {",
"            this.health += 8;     // 'this' is very important!",
"        }", 
"        else {",
"            this.health -= 10;    // 'this' is very important!",
"        }",
"        return isVictorious;",
"    },",
"",
"    /* isAlive returns whether requisiteWarrior is alive or not. */",
"    isAlive: function () {",
"        if (this.health >= 0) {  // 'this' is very important!",
"            return true;",
"        }", 
"        else {",
"            return false;",
"        }",
"	}",
"};",
"",
"// Game variables.",
"var isWon = null,  // stores result of last battle.",
"    msg = '';      // a temporary variable for composing messages.",
"",
"// Begin game. (Game ends when requisiteWarrior is no longer alive.)",
"window.alert('Do battle!');",
"",
"while (requisiteWarrior.isAlive()) {",
"    isWon = requisiteWarrior.doBattle();	// battle!",
"",
"    if (isWon) {",
"        msg = 'Victory! Health is up to ' + requisiteWarrior.health + '.\\nClick OK to do battle again.';",
"    }", 
"    else if (requisiteWarrior.isAlive()) {",
"        msg = 'Defeat. Health is down to ' + requisiteWarrior.health + '.\\nClick OK to do battle again.';",
"    }", 
"    else {",
"        msg = 'Alas, you have fought your last battle. :-(';",
"    }",
"    window.alert(msg);",
"}",
"",
"// Ending message",
"window.alert('You provided great service and were valued for you bravery.');"
    ],
    id: "object-requisiteWarrior"
});
</script>

### Constructors
* Sometimes, you need to create a bunch of objects that all have the same structure.
    * You might need several monsters.
    * You might need several pieces of candy.
* **Constructors** are special functions that when invoked with the `new` keyword will make a new object based on a prototype template.
<script>
makeTextArea({
    code: [
"/* Constructor for a Ball. */",
"function Ball(size, material, isHollow) {",
"    this.size = size;",
"    this.material = material;",
"    this.isHollow = isHollow;",
"}",
"",
"// Make some balls.",
"var myBall = new Ball(4, 'rubber', true);    // myBall is 4\", rubber, and hollow.",
"var yourBall = new Ball(6, 'foam', false);   // yourBall is 6\", foam, and solid.",
"var theShot = new Ball(5, 'iron', false);    // for use in shot put (http://bit.ly/1fq9z0V).",
"",
"window.alert('myBall\\'s size: ' + myBall.size);",
"window.alert('yourBall\\'s size: ' + yourBall.size);",
"window.alert('theShot\\'s size: ' + theShot.size);"
    ],
    id: "object-constructor-1"
});
</script>


* You might not be writing your own constructors for a while, so you don't fully need to understand what is happening in the definition of the constructor (i.e., the `Ball` function) above.
* However, most libraries and frameworks define constructors for you to use, so you must know how to use them.

<script>
window.onload = function () {
    activeNavLink('nav05');
}
</script>

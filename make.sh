#!/bin/bash

# ------------
# simple html5
# ------------
mkdir -p htdocs
for name in $(ls -1 *.md)
do
    outname=$(basename -s .md $name).html
    pandoc -Ss --self-contained \
    -f markdown \
    --toc \
    --toc-depth=4 \
    --include-in-header=includes/copyright-meta.html \
    --css=includes/codemirror.css \
    --css=includes/style.css \
    --include-in-header=includes/codemirror-compressed-link.html \
    --include-in-header=includes/mfk.js.html \
    --include-before-body=includes/navigation.html \
    --include-after-body=includes/footer.html \
    --include-after-body=includes/navigation.html \
    --highlight-style=haddock \
    -t html5 ${name} > htdocs/$outname
done

# Can't figure out how to get TOC to be links with markdown_strict+<extensions>
# Disable -f option for links.
    #~ -f markdown_strict+pandoc_title_block+escaped_line_breaks+inline_code_attributes \

# ------------------
# slidy (for slides)
# ------------------
#~ mkdir -p slides
#~ for name in $(ls -1 *.md)
#~ do
    #~ outname=$(basename -s .md $name).html
    #~ pandoc -Ss --self-contained \
    #~ --slide-level=3 \
    #~ --include-in-header=includes/copyright-meta.html \
    #~ --css=includes/codemirror.css \
    #~ --css=includes/style.css \
    #~ --css=includes/slidy-override.css \
    #~ --include-in-header=includes/codemirror-compressed-link.html \
    #~ --include-in-header=includes/mfk.js.html \
    #~ -f markdown_strict+pandoc_title_block+escaped_line_breaks \
    #~ -t slidy ${name} > slides/$outname
#~ done

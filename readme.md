Just Enough JavaScript
======================

"Just Enough Javascript" is an essentially (but not rigorously) correct crash course in JavaScript. It is intended to teach you just enough JavaScript so that you can start to program games using open source HTML5 game frameworks such as [enchant.js](http://enchantjs.com/), [Phaser](http://phaser.io/), and [melonJS](http://melonjs.org/). It assumes you don't know anything about programming.

"Just Enough Javascript" is Copyright © 2014 Mithat Konar. All rights reserved.

#! /bin/bash

CMD="php -S localhost:8080"

# start server
xterm -T "$CMD" -e $CMD -t htdocs/ &

# wait for server window to appear
rv=1
while [[ $rv  == 1 ]]
do
    wmctrl -F -a "$CMD"
    rv=$?
    #~ echo "sleeping..."
    sleep 0.1
done
# and put it in notification tray
(wmctrl -F -a "$CMD" && kdocker -f) &

# start browser
sleep 1
x-www-browser localhost:8080 &

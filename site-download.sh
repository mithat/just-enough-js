#! /bin/bash

# LOCAL, REMOTE, SERVER, USERNAME, PASSWORD are defined inside site.conf file.
source site.conf

# Global constants
LOGFILE=logs/download.log # log of lftp output
CMDFILE=site-download-cmd.tmp  # temp file in which lftp commands will be stored

# ask for permission:
read -p "Download files? [Y/n] " ans
case "$ans" in
    "" |  "y" |  "Y" | "yes" | "Yes" | "YES" )
        # continue #
        ;;
    *)
        exit 1;;
esac

# create a command file in the current dir and make it accessible only to user: 
touch ${CMDFILE}
chmod 600 ${CMDFILE}

# dump commands into command file:
echo "open -u ${USERNAME},${PASSWORD} ${SERVER}" > ${CMDFILE}
echo "mirror -c -v ${REMOTE} ${LOCAL}" >> ${CMDFILE}
echo "exit" >> ${CMDFILE}

### Frequently used options to mirror command when downloading:
###   -c: continue a mirror job if possible
###   -e: delete files not present at remote site
###   -v: verbosity level
###   --just-print, --dry-run   same as --script=-

# run the commands:
echo -ne "Running..."
lftp -f ${CMDFILE} &> ${LOGFILE}

# delete the command file:
rm -f ${CMDFILE}

# show logfile:
#~ numlines=$(wc -l ${LOGFILE})
#~ if [[ $numlines < 20 ]]; then
    #~ echo "Session log"
    #~ echo "-----------"
    #~ cat ${LOGFILE}
#~ else
    pager ${LOGFILE}
#~ fi

echo "done"
